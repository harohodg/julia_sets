#!/usr/bin/env python

from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.pyplot as plt
import numpy as np
import argparse


#Setup program argument parser
parser = argparse.ArgumentParser(description='Julia Set Plot Image creator')
parser.add_argument('data_file', help = 'What data file arer we using?' )
parser.add_argument('data_dim', help = 'The data dimensions "r,c"')
parser.add_argument('--data_type', choices=['uint8','uint16','uint32'], default='uint8',help='What format is the data in?')
parser.add_argument('-c', '--cm', default='copper', choices = sorted(m for m in plt.cm.datad if not m.endswith("_r")), help='Which color map to use. Default=copper' )
parser.add_argument('-o', '--out_file',  default = 'Julia.png', help = 'What is the output file name?' )
parser.add_argument('-dx', '--down_sample_x', default=1, type=int, help='Use every nth column of the raw data.')
parser.add_argument('-dy', '--down_sample_y', default=1, type=int, help='Use every nth row of the raw data.')

    
#get program arguments
args = parser.parse_args()
print(args)


with open( args.data_file ) as f:
    data = np.fromfile( f, dtype=args.data_type )
    data = data.reshape( np.array(args.data_dim.split(',')).astype(int) )
    data = data[::args.down_sample_y,::args.down_sample_x] 

X,Y = np.meshgrid( np.linspace(-3,3,data.shape[0]), np.linspace(-3,3,data.shape[1]) )
X = X.flatten()
Y = Y.flatten()
data = data.flatten()

fig = plt.figure()
ax = fig.gca(projection='3d')
ax.set_axis_off()

ax.plot_trisurf(X, Y, data, cmap=args.cm, linewidth=0.2)
plt.savefig(args.out_file)
