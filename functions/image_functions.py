from __future__ import division
def percent_red(image, iterations, max_i):
    "Pixel is a percentage of red based on percentage of max iterations"
    image += ( (0xFF*iterations.astype(float)/max_i).astype(int)+0xFF000000).astype('int32')


def percent_shift(image, iterations, max_i):
    "left shift the interation value by a 24 * the percentage it is of the max"
    image += ( (iterations<<( (24*iterations.astype(float)/max_i ).astype(int) ) ) + 0xFF000000 ).astype("int32")
    
def black_and_white(image, iterations, max_i):
    "Stable points are black, unstable white"    
    image += ( (0xFFFFFF*(1 - (iterations//max_i) ) ) + 0xFF000000).astype("int32")

def shift_then_percent(image, iterations, max_i):
    "shift it then percent it"
    image += ( ( (0xFF<<( (24*iterations.astype(float)/max_i ).astype(int) ) )*iterations.astype(float)/max_i ).astype(int) + 0xFF000000 ).astype("int32")  

def color_map(image, iterations, color_map):
    'Use iterations as index to color map'
    colors = color_map(iterations.astype(int) )
    colors = colors.reshape( (colors.shape[0]*colors.shape[1],colors.shape[2]) )
    colors = (0xFF*colors).astype(int)
    image += ( sum( colors[:,i]<<(8*i) for i in range(4) ) ).reshape(image.shape)
        
functions  = {"percent_red":percent_red,
              "percent_shift":percent_shift,
              "black_and_white": black_and_white,
              "shift_then_percent":shift_then_percent,
              'color_map':color_map
             }   
