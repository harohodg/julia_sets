import struct
BINARY_HEADER = "80sI"
BINARY_FACET  = "12fH"
BINARY_OFFSET = 84

ASCII_FACET = """  facet normal  {face[0]:e}  {face[1]:e}  {face[2]:e}
    outer loop
      vertex    {face[3]:e}  {face[4]:e}  {face[5]:e}
      vertex    {face[6]:e}  {face[7]:e}  {face[8]:e}
      vertex    {face[9]:e}  {face[10]:e}  {face[11]:e}
    endloop
  endfacet"""


class STL_Writer:
    '''
    Simple class to export stl files. Based on 
    stlwriter.py ....
    numpy2stl  ....
    '''
    def __init__(self, fname, ascii=False):
        self.fp = open(fname, 'wb')
        self.num_facets = 0
        self.ascii = ascii
        
        if self.ascii:
            self.fp.write( 'solid ffd_geom\n'.encode("UTF-8") )
        else:
            self.fp.seek( BINARY_OFFSET )
    
    def _write_binary_header(self):
        self.fp.seek(0)
        header=struct.pack(BINARY_HEADER, b'Binary STL Writer', self.num_facets)
        self.fp.write( header )
        self.fp.close()
        
        
    def add_face(self,face):
        self.num_facets += 1
        if not self.ascii:
            facet = list(face)
            facet.append(0)  # need to pad the end with a unsigned short byte
            self.fp.write( struct.pack(BINARY_FACET, *facet) )
        else:
            facet = ASCII_FACET.format(face=face)+'\n'
            self.fp.write( facet.encode("UTF-8") )
    
    def add_faces(self, faces):
        for face in faces:
            self.add_face(face)
    
    def close(self):
        if not self.ascii:
            self._write_binary_header()
        else:
            self.fp.write( 'endsolid ffd_geom'.encode("UTF-8") )
            self.fp.close()
