import numpy as np
def create_face(xy_pairs, height_values):
    #points should be defined clockwise as seen from the outside of the model
    face = []
    for index, value in enumerate(xy_pairs):
        face.append( value + (height_values[index],) )
    return face
