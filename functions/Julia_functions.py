import numpy as np

'''
Functions for computing the orbits of various Julia Sets.
Specifically polynomial and trig
'''

POLY_CUTOFF = 2
TRIG_CUTOFF = 50
MAX_ITERATIONS = 50
DEFAULT_N = 2   
    
    
def compute_poly_orbits(vals, iterations, c , n=DEFAULT_N, max_iterations=MAX_ITERATIONS, cutoff = POLY_CUTOFF):
    '''
    Function to compute the orbits of z^n + c functions
    
    Inputs :
        vals : the array of initial orbit values
        iterations : the array to keep track or infinity escape iterations
        c  : the complex number to add
        n  : the power to raise z to before adding c                
        max_iterations : when to stop iterating
        cutoff : the infinity threshold
    '''
    for i in range(max_iterations):
        vals = vals**n + c   

        #replace values going to infinity with nan       
        np.place( vals, abs(vals)>=cutoff, np.nan )
        
        #update num iterations
        iterations += np.logical_not( np.isnan(vals) )
    return 
    
    
def compute_trig_orbits(vals, iterations, f, c, max_iterations=MAX_ITERATIONS, cutoff = TRIG_CUTOFF):
    '''
    Function to compute the orbits of c*trig(x) functions where trig is sin or cos
        
    Inputs :
        vals : the array of initial orbit values
        iterations : the array to keep track or infinity escape iterations
        f  : the function to apply 'sin' or 'cos'
        c  : the complex number to add        
        max_iterations : when to stop iterating
        cutoff : the infinity threshold
    '''
    for i in range(max_iterations):
        if f == 'sin':
            vals = c*np.sin(vals)
        else:
            vals = c*np.cos(vals)   
        
        #replace values going to infinity with nan       
        np.place( vals, abs(vals.imag)>=cutoff, np.nan )
        
        #update num iterations
        iterations += np.logical_not( np.isnan(vals) )
    return    
