#!/usr/bin/env python

from __future__ import print_function
from __future__ import division
import argparse
import matplotlib.pyplot as plt
from PIL import Image
import numpy as np
from mpi4py import MPI
from functions.image_functions import functions


run_time = MPI.Wtime()

#Setup program argument parser
parser = argparse.ArgumentParser(description='Julia Set image creator')
parser.add_argument('data_file', help = 'What data file arer we using?', nargs='+' )
parser.add_argument('data_dim', help = 'The data dimensions "r,c"')
parser.add_argument('--data_type', choices=['uint8','uint16','uint32'], default='uint8',help='What format is the data in?')
parser.add_argument('-f', '--function', default="black_and_white", choices=functions.keys(), help='Which function are we applying default = black_and_white' )
parser.add_argument('-o', '--out_file',  default = 'Julia.png', help = 'What is the output file name?' )
parser.add_argument('-dx', '--down_sample_x', default=1, type=int, help='Use every nth column of the raw data.')
parser.add_argument('-dy', '--down_sample_y', default=1, type=int, help='Use every nth row of the raw data.')

parser.add_argument('-cm', '--color_map', default='copper', choices = sorted(m for m in plt.cm.datad if not m.endswith("_r")), help='Color map to use if using color map function.')
parser.add_argument('-m', '--max',  type=int, help='Value to scale everything to.')

#get program arguments
args = parser.parse_args()
print(args)

#Get MPI parameters
comm	  = MPI.COMM_WORLD
rank      = comm.rank
num_procs = comm.size


#Get raw data and broadcast to everyone else
if rank == 0:    
    with open( args.data_file[0] ) as f:
        iterations_data = np.fromfile( f, dtype=args.data_type )
        iterations_data = iterations_data.reshape( np.array(args.data_dim.split(',')).astype(int) )
        iterations_data = iterations_data[::args.down_sample_y,::args.down_sample_x]
        iterations_data = ( iterations_data.astype(float) * (args.max/iterations_data.astype(float).max() ) ).astype(int) if args.max is not None else iterations_data
    for data_file in args.data_file[1:]:
        with open( data_file ) as f:
            tmp_data = np.fromfile( f, dtype=args.data_type )
            tmp_data = tmp_data.reshape( np.array(args.data_dim.split(',')).astype(int) )
            tmp_data = tmp_data[::args.down_sample_y,::args.down_sample_x]
            
            iterations_data += tmp_data
    iterations_data = ( iterations_data.astype(float)/len(args.data_file) ).astype(int)                
    iterations_data = ( iterations_data.astype(float) * (args.max/iterations_data.astype(float).max() ) ).astype(int) if args.max is not None else iterations_data
         
else:
    iterations_data = np.zeros( np.array(args.data_dim.split(',')).astype(int), dtype=args.data_type)
comm.Bcast(iterations_data)    

#----------------------------------------------------------------
#local computation setup
setup_time = -MPI.Wtime()
    
max_i = iterations_data.max()    
func = functions[ args.function ]    

h, w = iterations_data.shape
local_image = np.zeros((h,w),np.uint32)

num_local_rows = h // num_procs + (h % num_procs > rank)
N = num_local_rows

f_index = comm.scan(num_local_rows) - num_local_rows  

setup_time += MPI.Wtime()


#-------------------------------------
#Actual computation
local_compute_time = -MPI.Wtime()

if args.function != 'color_map':
    func( local_image[f_index:f_index+N,:], iterations_data[f_index:f_index+N,:], max_i)
else:
    func( local_image[f_index:f_index+N,:], iterations_data[f_index:f_index+N,:], plt.get_cmap(args.color_map))
    

local_compute_time += MPI.Wtime()


#-----------------------------------
#Merge Final Results
reduction_time = -MPI.Wtime()

final_image = np.zeros((h,w), local_image.dtype) if rank == 0 else None
comm.Reduce( local_image, final_image )

reduction_time += MPI.Wtime()


#get timings
global_compute = comm.reduce( local_compute_time, MPI.SUM ) 
global_setup   = comm.reduce( setup_time, MPI.SUM ) 



#--------------------------------------
#And Export
if rank == 0:
    export_time = -MPI.Wtime()
                
    pilImage = Image.frombuffer('RGBA', final_image.shape, final_image,'raw','RGBA',0,1)
    pilImage.save( args.out_file )
    
    export_time += MPI.Wtime()
    
    average_compute_time = global_compute/num_procs
    results = {"data_config" : args,
               "acc" : average_compute_time,
               "acr" : average_compute_time/h,
               "acp" : average_compute_time/(w*h),
               "np"  : num_procs,
               "as"  : global_setup/num_procs,
               "et"  : export_time,
               "rt"  : MPI.Wtime() - run_time 
              }
    
    with open('{0}_stats'.format(args.out_file),'w') as f:
        print(results, file=f)
        
