#!/usr/bin/env python

from __future__ import print_function
from __future__ import division
import argparse
import numpy as np
from mpi4py import MPI
import cPickle as pickle
from functions.Julia_functions import *
import numpy as np


run_time = MPI.Wtime()

#Setup program argument parser
parser = argparse.ArgumentParser(description='Julia Set data creator')
parser.add_argument('-m', '--max_iterations', default=MAX_ITERATIONS, type=int, help='How many iterations to apply before quitting. default={0}'.format(MAX_ITERATIONS) )
parser.add_argument('-f', '--function', default='poly', choices=['poly','sin','cos'], help='Which function are we applying default = poly' )
parser.add_argument('-p', '--power', default='2', help='Power to use with the polynomial function default = {0}. (use #,#,# for range (min,max,n values)'.format(DEFAULT_N) )

parser.add_argument('-c', '--c', default=complex('0+0j'), type=complex, help='Value of c to apply ("#+#j") Warning may crash. Use c1,c2.' )
parser.add_argument('-c1','--c1', help='C1 value.(use #,#,# for range (min,max,n values)')
parser.add_argument('-c2','--c2', help='C2 value.(use #,#,# for range (min,max,n values)')

parser.add_argument('-t', '--threshold', type=int, help='Cutoff value to use if you want to override given values default for trig : {0} for poly_n : {1} '.format(POLY_CUTOFF, TRIG_CUTOFF) )

parser.add_argument('-o', '--out_file', default="iterations_data", help='File name to use for data and stats dump.' )
parser.add_argument('--data_type', choices=['uint8','uint16','uint32'], default='uint8',help='Iterations data type, choose according to max iterations default(uint8)')

parser.add_argument('--min_x', default=-3, type=float, help='Min x value to use. default = -3' )
parser.add_argument('--max_x', default=3,  type=float, help='Max x value to use. default = 3' )
parser.add_argument('--min_y', default=-3, type=float, help='Min y value to use. default = -3' )
parser.add_argument('--max_y', default=3,  type=float, help='Max y value to use. default = 3' )

parser.add_argument('--num_x', default=100, type=int, help='Number of x values to use. default = 100' )
parser.add_argument('--num_y', default=100, type=int, help='Number of y values to use. default = 100' )
    
#get program arguments
args = parser.parse_args()
print(args)

#Get MPI parameters
comm	  = MPI.COMM_WORLD
rank      = comm.rank
num_procs = comm.size


#--------------------------------------
#local setup for computing
setup_time = -MPI.Wtime()


if args.threshold is None:
    args.threshold = POLY_CUTOFF if args.function == 'poly' else TRIG_CUTOFF
    
    
if args.c1 is not None and args.c2 is not None:
    if ',' in args.c1:
        c1_chunks = np.array( args.c1.split(',') ).astype(float)
        c1_range  = np.linspace(c1_chunks[0], c1_chunks[1], c1_chunks[2] )
    else:
        c1_range = [ float(args.c1) ]
    
    if ',' in args.c2:
        c2_chunks = np.array( args.c2.split(',') ).astype(float)
        c2_range  = np.linspace(c2_chunks[0], c2_chunks[1], c2_chunks[2] )
    else:
        c2_range = [ float(args.c2) ]   
    
else:
    c1_range = [args.c.real]
    c2_range = [args.c.imag]

if ',' in args.power:
    p_chunks = np.array( args.power.split(',') ).astype(float)
    p_range  = np.linspace(p_chunks[0], p_chunks[1], p_chunks[2] )
else:
    p_range = [ float(args.power) ]    

c1_vals, c2_vals, p_vals = np.meshgrid( c1_range, c2_range, p_range )
parameters = np.dstack( (c1_vals.flatten(), c2_vals.flatten(), p_vals.flatten() ) )
parameters = parameters.reshape( (parameters.shape[1], parameters.shape[2]) )

#Calculate x and y values for the computation grid
xvals = np.linspace( args.min_x, args.max_x, args.num_x )
yvals = np.linspace( args.max_y, args.min_y, args.num_y )


# calculate number of rows to compute here
num_local_rows = args.num_y // num_procs + (args.num_y % num_procs > rank)
N = num_local_rows

#and what the first row index is
f_index = comm.scan(num_local_rows) - num_local_rows


#Compute actual mesh
x_values, y_values = np.meshgrid( xvals, yvals )
    
    
setup_time += MPI.Wtime()


#Could generate a massive matrix and let each chunk to their thing 
#But that would take a long more memory
for slice_num, parameter_values in enumerate(parameters):
    c1, c2, p = parameter_values
    c = complex(c1,c2)


    #Set aside space for the orbit values
    vals = np.zeros( (args.num_y, args.num_x), dtype=np.complex )
    vals[f_index:f_index+N,:] = x_values[f_index:f_index+N,:] + y_values[f_index:f_index+N,:]*1j

    #Setup the array for keeping track of iterations till the orbits escape to infinity
    local_iterations = np.zeros( vals.shape, dtype=args.data_type)

    #---------------------------------------------------------
    #Actual Computation
    local_compute_time = -MPI.Wtime()

    if args.function == 'poly':
        compute_poly_orbits(vals[f_index:f_index+N,:], local_iterations[f_index:f_index+N,:], c , p, args.max_iterations, args.threshold)
    else:
        compute_trig_orbits(vals[f_index:f_index+N,:], local_iterations[f_index:f_index+N,:], args.function, c, args.max_iterations, args.threshold)
    local_compute_time += MPI.Wtime()

    #------------------------------------
    #Merge final results
    reduction_time = -MPI.Wtime()
    global_iterations = np.zeros( local_iterations.shape, dtype=local_iterations.dtype) if rank == 0 else None
    comm.Reduce(local_iterations, global_iterations)
    reduction_time += MPI.Wtime()

    #get timings
    global_reduction = comm.reduce( reduction_time, MPI.SUM )     
    global_compute   = comm.reduce( local_compute_time, MPI.SUM )
    global_setup     = comm.reduce( setup_time, MPI.SUM ) 

    if rank == 0:
        export_time = -MPI.Wtime()
        
        if len(parameters) != 1:
            f = open("{0}_slice_{1}.bin".format(args.out_file, slice_num), "w")
        else:
            f = open("{0}.bin".format(args.out_file), "w")
        global_iterations.tofile( f )
        f.close()
               
        export_time += MPI.Wtime()
        
        
        
#        average_compute_time = global_compute/num_procs
#        results = {"data_config" : args,
#                   "acc" : average_compute_time,
#                   "acr" : average_compute_time/args.num_y,
#                   "acp" : average_compute_time/(args.num_x*args.num_y),
#                   "np"  : num_procs,
#                   "as"  : global_setup/num_procs,
#                   "et"  : export_time,
#                   "rt"  : MPI.Wtime() - run_time,
#                   'data_type': global_iterations.dtype
#                  }
#        
#        with open("{0}.stats".format(args.out_file),"w") as f:
#            print(results, file=f)
 

