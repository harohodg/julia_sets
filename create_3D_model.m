#!/usr/bin/env python

'''
Code to create 3D models of Julia set data.

Note : This code should not be trusted. 
Please run through a fixer before attempting to print.
'''
from __future__ import division
import numpy as np
import argparse
from functions.STL_Writer import STL_Writer


#Setup program argument parser
parser = argparse.ArgumentParser(description='Julia Set 3D model creator')
parser.add_argument('data_file', help = 'What data file arer we using?' )
parser.add_argument('data_dim', help = 'The data dimensions "r,c"')
parser.add_argument('-x', '--x_scale', default=1, type=float, help='How big is each pixel in the x direction (mm).')
parser.add_argument('-y', '--y_scale', default=1, type=float, help='How big is each pixel in the y direction (mm).')
parser.add_argument('-z', '--z_scale', default=1, type=float, help='What do we mutiply each height value by?')
parser.add_argument('-o', '--out_file',  default = 'Julia.stl', help = 'What is the output file name?' )
parser.add_argument('-t', '--file_type', default = 'ascii', choices= ['ascii','binary'], help='Output file format')
parser.add_argument('-b', '--base', default=False, action='store_true', help='Add a base to the model?')
parser.add_argument('-u', '--unique', default=False, action='store_true', help='Show list of unique data values?')
parser.add_argument('-m', '--max', default=np.inf, type=float, help='Max value of model? All values >= max will become max')
parser.add_argument('-dx', '--down_sample_x', default=1, type=int, help='Use every nth column of the raw data.')
parser.add_argument('-dy', '--down_sample_y', default=1, type=int, help='Use every nth row of the raw data.')
   
#get program arguments
args = parser.parse_args()
print(args)


with open( args.data_file ) as f:
    data = np.fromfile( f, dtype=np.uint8 ).reshape( np.array(args.data_dim.split(',')).astype(int) )
    data = data.astype(float)
    data += 1.0
    data *= args.z_scale
    data = data[::args.down_sample_y,::args.down_sample_x]
    
    if args.unique:
        print( np.unique(data) )
        
    data[ np.where(data>= args.max) ] = args.max    
writer = STL_Writer(args.out_file, ascii=(args.file_type=='ascii') )    

num_y, num_x = data.shape

x_edge = (num_x//2)*args.x_scale + args.x_scale/2
y_edge = (num_y//2)*args.y_scale + args.y_scale/2

x_grid = np.linspace(-x_edge, x_edge, num_x+1)
y_grid = np.linspace(y_edge, -y_edge, num_y+1)



left_x  = np.column_stack( (x_grid[0:-1],)*num_x ).transpose()
right_x = np.column_stack( (x_grid[1:],  )*num_x ).transpose()

top_y   =  np.column_stack( (y_grid[:-1],)*num_y )
bot_y   =  np.column_stack( (y_grid[1:],  )*num_y )

#top faces points #All have norm (0,0,1)
p4 = np.dstack( (left_x,  bot_y, data) )
p3 = np.dstack( (left_x,  top_y, data) )
p2 = np.dstack( (right_x, top_y, data) )
p1 = np.dstack( (right_x, bot_y, data) )

norm = np.array( [0,0,1]*(p1.shape[0]*p1.shape[1]) ).reshape( p1.shape )
#Top face triangles (defined counter clockwise)
triangles_1 = np.dstack( (norm, p1,p2,p3) )
triangles_2 = np.dstack( (norm, p1,p3,p4) )
triangles = np.vstack( (triangles_1, triangles_2) )


#Export top triangles
triangles = triangles.reshape( ( triangles.shape[0]*triangles.shape[1],12) )
writer.add_faces(triangles)

#-------------------------------------------------------------
#front face points (Not including bottom row)
#-------------------------------------------------------------
p4 = np.dstack( ( left_x[:-1,:],  bot_y[:-1,:],  data[1:,:]) )#use next row down for h
p3 = np.dstack( ( left_x[:-1,:],  bot_y[:-1,:],  data[:-1,:]) )
p2 = np.dstack( (right_x[:-1,:],  bot_y[:-1,:],  data[:-1,:]) )
p1 = np.dstack( (right_x[:-1,:],  bot_y[:-1,:],  data[1:,:]) )

norm = np.array( [0,-1,0]*(p1.shape[0]*p1.shape[1]) ).reshape( p1.shape )

#front triangles (all but bottom row)
triangles_1 = np.dstack( (norm, p1,p2,p3) )
triangles_2 = np.dstack( (norm, p1,p3,p4) )
triangles = np.vstack( (triangles_1, triangles_2) )

#Export front triangles
#first remove triangles which are lines (first and second z values are the same)
triangles = triangles.reshape( (-1,12) )
triangles = np.delete(triangles, np.where( triangles[:,5]==triangles[:,8] ), axis=0)


writer.add_faces(triangles)


#-------------------------------------------------------------
#front face points (Botttom row)
#-------------------------------------------------------------
p4 = np.dstack( (left_x[-1,:], bot_y[-1,:], np.zeros( (1,num_x) )) )
p3 = np.dstack( (left_x[-1,:], bot_y[-1,:], data[-1,:]) )
p2 = np.dstack( (right_x[-1,:], bot_y[-1,:], data[-1,:]) )
p1 = np.dstack( (right_x[-1,:], bot_y[-1,:], np.zeros( (1,num_x) )) )

norm = np.array( [0,-1,0]*(p1.shape[0]*p1.shape[1]) ).reshape( p1.shape )

#front triangles (bottom row)
triangles_1 = np.dstack( (norm, p1,p2,p3) )
triangles_2 = np.dstack( (norm, p1,p3,p4) )
triangles = np.vstack( (triangles_1, triangles_2) )

#Export front triangles
triangles = triangles.reshape( ( triangles.shape[0]*triangles.shape[1],12) )
writer.add_faces(triangles)


#------------------------------------------------
#left hand face (all but first column)
p4 = np.dstack( (left_x[:,1:], top_y[:,1:], data[:,:-1] ) )#all but last column
p3 = np.dstack( (left_x[:,1:], top_y[:,1:], data[:,1:] ) )
p2 = np.dstack( (left_x[:,1:], bot_y[:,1:], data[:,1:] ) )
p1 = np.dstack( (left_x[:,1:], bot_y[:,1:], data[:,:-1] ) )

norm = np.array( [-1,0,0]*(p1.shape[0]*p1.shape[1]) ).reshape( p1.shape )
#left side middle triangles
triangles_1 = np.dstack( (norm, p1,p2,p3) )
triangles_2 = np.dstack( (norm, p1,p3,p4) )
triangles = np.vstack( (triangles_1, triangles_2) )

#export left side middle triangles
#first remove triangles which are lines (first and second z values are the same)
triangles = triangles.reshape( (-1,12) )
triangles = np.delete(triangles, np.where( triangles[:,5]==triangles[:,8] ), axis=0)

writer.add_faces(triangles)


#------------------------------------------------
#left hand face (first column)
p4 = np.dstack( (left_x[:,1], top_y[:,1], np.zeros( (num_y,) ) ) )
p3 = np.dstack( (left_x[:,1], top_y[:,1], data[:,1]) )
p2 = np.dstack( (left_x[:,1], bot_y[:,1], data[:,1]) )
p1 = np.dstack( (left_x[:,1], bot_y[:,1], np.zeros( (num_y,) ) ) )

norm = np.array( [-1,0,0]*(p1.shape[0]*p1.shape[1]) ).reshape( p1.shape )

#left side first column triangles
triangles_1 = np.dstack( (norm, p1,p2,p3) )
triangles_2 = np.dstack( (norm, p1,p3,p4) )
triangles = np.vstack( (triangles_1, triangles_2) )

#export left side first column triangles
triangles = triangles.reshape( ( triangles.shape[0]*triangles.shape[1],12) )
writer.add_faces(triangles)




#-----------------------------------------------
#Top face (not including first row)
p4 = np.dstack( (right_x[1:,:], top_y[1:,:], data[:-1,:]) )#use row above for h
p3 = np.dstack( (right_x[1:,:], top_y[1:,:], data[1:,:]) )
p2 = np.dstack( (left_x[1:,:], top_y[1:,:], data[1:,:]) )
p1 = np.dstack( (left_x[1:,:], top_y[1:,:], data[:-1,:]) )

norm = np.array( [0,1,0]*(p1.shape[0]*p1.shape[1]) ).reshape( p1.shape )

#top face triangles (not including first row)
triangles_1 = np.dstack( (norm, p1,p2,p3) )
triangles_2 = np.dstack( (norm, p1,p3,p4) )
triangles = np.vstack( (triangles_1, triangles_2) )

#export top face (not first row) triangles
#first remove triangles which are lines (first and second z values are the same)
triangles = triangles.reshape( (-1,12) )
triangles = np.delete(triangles, np.where( triangles[:,5]==triangles[:,8] ), axis=0)

writer.add_faces(triangles)

#--------------------------------------------
#Top face (first row)
p4 = np.dstack( (right_x[1,:], top_y[1,:], np.zeros( (1,num_x) ) ) )
p3 = np.dstack( (right_x[1,:], top_y[1,:], data[1,:]) )
p2 = np.dstack( (left_x[1,:], top_y[1,:], data[1,:]) )
p1 = np.dstack( (left_x[1,:], top_y[1,:], np.zeros( (1,num_x) ) ) )

norm = np.array( [0,1,0]*(p1.shape[0]*p1.shape[1]) ).reshape( p1.shape )

#top face triangles (first row)
triangles_1 = np.dstack( (norm, p1,p2,p3) )
triangles_2 = np.dstack( (norm, p1,p3,p4) )
triangles = np.vstack( (triangles_1, triangles_2) )

#export top face (first row) triangles
triangles = triangles.reshape( ( triangles.shape[0]*triangles.shape[1],12) )
writer.add_faces(triangles)



#------------------------------------------------
#Right side face (not includeing last colum)
p4 = np.dstack( (right_x[:,:-1], bot_y[:,:-1], data[:,1:]) )#use next column to right for h
p3 = np.dstack( (right_x[:,:-1], bot_y[:,:-1], data[:,:-1]) )
p2 = np.dstack( (right_x[:,:-1], top_y[:,:-1], data[:,:-1]) )
p1 = np.dstack( (right_x[:,:-1], top_y[:,:-1], data[:,1:]) )

norm = np.array( [1,0,0]*(p1.shape[0]*p1.shape[1]) ).reshape( p1.shape )

#Right face triangles (last column not included)
triangles_1 = np.dstack( (norm, p1,p2,p3) )
triangles_2 = np.dstack( (norm, p1,p3,p4) )
triangles = np.vstack( (triangles_1, triangles_2) )

#export right face (not including last column)
#first remove triangles which are lines (first and second z values are the same)
triangles = triangles.reshape( (-1,12) )
triangles = np.delete(triangles, np.where( triangles[:,5]==triangles[:,8] ), axis=0)

writer.add_faces(triangles)


#---------------------------------------------------
#Right side face (last column)
p4 = np.dstack( (right_x[:,-1], bot_y[:,-1], np.zeros( (num_y,) ) ) )
p3 = np.dstack( (right_x[:,-1], bot_y[:,-1], data[:,-1]) )
p2 = np.dstack( (right_x[:,-1], top_y[:,-1], data[:,-1]) )
p1 = np.dstack( (right_x[:,-1], top_y[:,-1], np.zeros( (num_y,) ) ) )

norm = np.array( [1,0,0]*(p1.shape[0]*p1.shape[1]) ).reshape( p1.shape )

#Right face triangles (last column)
triangles_1 = np.dstack( (norm, p1,p2,p3) )
triangles_2 = np.dstack( (norm, p1,p3,p4) )
triangles = np.vstack( (triangles_1, triangles_2) )

#export right face (last column)
triangles = triangles.reshape( ( triangles.shape[0]*triangles.shape[1],12) )
writer.add_faces(triangles)

if args.base:
    p4 = np.array( [x_grid[0], y_grid[-1], 0] )
    p3 = np.array( [x_grid[0], y_grid[0], 0] )
    p2 = np.array( [x_grid[-1], y_grid[0], 0] )
    p1 = np.array( [x_grid[-1], y_grid[-1], 0] )
    norm = np.array( [0,0,-1] )

    triangles_1 = np.concatenate( (norm, p1,p2,p3) )
    triangles_2 = np.concatenate( (norm, p1,p3,p4) )
    triangles = np.vstack( (triangles_1, triangles_2) )
    
    triangles = triangles.reshape( (-1,12) )
    print(triangles)
    
    writer.add_faces(triangles)
    
writer.close()
